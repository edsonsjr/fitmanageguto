<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $fillable = ['cli_name', 'cli_doc', 'cli_end', 'cli_nasc', 'cli_tel', 'cli_email'];
}
