@extends('layouts.admin')

@section('content')
    
    <painel-side>
        <panel-crud>
            <table-client url="{{route('client.create')}}" :itens="{{$clients}}"></table-client>
        </panel-crud>
    </painel-side>
    
@endsection
