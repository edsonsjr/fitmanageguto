<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'cli_name' => ['required', 'string', 'max:255'],
            'cli_doc' => ['required', 'string', 'max:255'],
            'cli_end' => ['required', 'string', 'max:255'],
            'cli_nasc' => ['required', 'date', 'max:255'],
            'cli_tel' => ['required', 'string', 'max:255'],
            'cli_email' => ['required', 'string', 'email', 'max:255'],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $clients = json_encode(Client::all());

        return view('admin.client.list', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.client.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Get all data from form
        $data = $request->all();
        
        // Create new form validator to check if the rules
        // was respected
        $validator = $this->validator($data);

        if($validator->fails())
        {

            // Redirect to register view with the errors
            return redirect()->back()
                    ->withErrors($validator->errors())
                    ->withInput();
        }

        // If has no errors, create new client in DB
        $newClient = json_encode(Client::create($data));

        // Return the clients list view
        return view('admin.client.list', compact('newClient'));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }
}
